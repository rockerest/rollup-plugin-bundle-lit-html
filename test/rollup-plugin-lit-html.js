var assert = require( "assert" );

var rollup = require( "rollup" );

var resolve = require( "rollup-plugin-node-resolve" );
var lit = require( "../dist/rollup-plugin-lit-html.cjs.js" );

var rollupOptions = {
	"input": "test/fixtures/test.js",
	"plugins": [
		resolve(),
		lit()
	]
};

var rolloutOptions = {
	"name": "blh",
	"format": "iife",
	"intro": "var window = {};"
};

describe( "rollup-plugin-lit-html", () => {
	it( "should return a template function for the HTML file", async () => {
		var rolledup = await rollup.rollup( rollupOptions );
		var { output } = await rolledup.generate( rolloutOptions );

		var runnable = new Function( "assert", output[ 0 ].code );

		runnable( assert );
	} );

	it( "should properly bundle complicated nested templates", async () => {
		var rolledup = await rollup.rollup( Object.assign( {}, rollupOptions, {
			"input": "test/fixtures/nested.js"
		} ) );
		var { output } = await rolledup.generate( rolloutOptions );

		var runnable = new Function( "assert", output[ 0 ].code );

		runnable( assert );
	} );

	it( "should fail when the HTML import is excluded (because there's no loader for that file)", async () => {
		return assert.rejects( () => rollup.rollup( Object.assign( {}, rollupOptions, {
			"input": "test/fixtures/fail.js",
			"plugins": [
				lit( {
					"exclude": [ "**/static.html" ]
				} )
			] } )
		) );
	} );
} );