import { TemplateResult, html } from "../../node_modules/lit-html/lit-html.js";

import { template } from "./nested.html";

var lit = template( { html } );

assert.equal( typeof template, "function" );
assert.ok( lit instanceof TemplateResult );
assert.equal( lit.type, "html" );

assert.equal( lit.strings.length, 3 );
assert.equal( lit.strings[ 0 ], "This has " );
assert.equal( lit.strings[ 1 ], " nested template literals.\n<br />\n<br />\nLike " );
assert.equal( lit.strings[ 2 ], " nested template literals." );

assert.ok( lit.values[ 0 ] instanceof Array );
assert.ok( lit.values[ 1 ] instanceof Array );

assert.equal( lit.values[ 0 ].length, 3 );
assert.equal( lit.values[ 1 ].length, 2 );

[ ...lit.values[ 0 ], ...lit.values[ 1 ] ].forEach( ( value ) => {
	assert.ok( value instanceof TemplateResult );
} );